import org.apache.log4j.Logger;
import org.junit.After;
import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.Test;
import org.junit.Before;
import org.openqa.selenium.interactions.Actions;
import java.nio.channels.SelectableChannel;
import java.util.*;


public class ImportTrello {
    WebDriver driver;
    MainPage element;
    TrelloItems trello;


    private static final Logger log = Logger.getLogger(ImportTrello.class);




    @Before
    public void LoginInAtlaz() throws Exception {
        System.setProperty("webdriver.chrome.driver", "d:\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(element.url);
        element = new MainPage(driver);
        trello = new TrelloItems(driver);
        element.RegistrationInAtlaz(element);

        //код для выбора определенной компании
        /*for(int i = 1; i < 10; i ++){
            String a = "//choose-compnay/a[" + i + "]/div[2]";
            String NameOfElement = driver.findElement(By.xpath(a)).getText();
            if(NameOfElement.equals("ForImportKsu")){
                String aq = "//choose-compnay/a[" + i + "]";
                WebElement select =  driver.findElement(By.xpath(aq));
                select.click();
                break;}
        }*/

        element.ClickButton(element.UserMenuBlock);
        element.ClickButton(element.CompanySettingsButton);
        element.ClickButton(element.Import);

    }

    @Test
    public void ImportTrelloWithOutUsers() throws Exception {
        System.out.println("Тест :Импорт трелло без приглашения юзеров");
            trello.Registration(trello);
            element.Waiter(element.ListOfProjects);
            WebElement randomProject = trello.Projects.get(new Random().nextInt(trello.Projects.size()));
            randomProject.click();
            element.ClickButton(element.ContinueImportButton);
            trello.ClickButton(trello.SkipButtonTrello);
            element.ClickButton(element.OkGotItButtonAfterImport);
            trello.CheckNotification(trello);

    }

   @Test
    public void ImportTrelloWithUsers() throws Exception {
       System.out.println("Тест :Импорт трелло с приглашением юзеров");



            trello.Registration(trello);
            element.Waiter(element.ListOfProjects);
            WebElement randomProject = trello.Projects.get(new Random().nextInt(trello.Projects.size()));
            randomProject.click();
            element.ClickButton(element.ContinueImportButton);
            Thread.sleep(500);
            trello.ListOfInvatingUsers(trello, driver);
            /*for (int i = 17; i < trello.ListOfUsers.size(); i++) {
                WebElement a = trello.ListOfUsers.get(i);
                a.click();
                Thread.sleep(500);
                (new Actions(driver)).sendKeys(i + "tralala@ya.ru").build().perform();
            }*/

            trello.ClickButton(trello.ContinueImportAfterSelectingUsers);
            element.ClickButton(element.OkGotItButtonAfterImport);
            trello.CheckNotification(trello);

    }





    @Test
    public void ImportTrelloBackButton() throws Exception {
        System.out.println("Тест :Завершение импорта с помощью кнопки Back");


        trello.Registration(trello);
        element.Waiter(element.ListOfProjects);
        WebElement randomProject = trello.Projects.get(new Random().nextInt(trello.Projects.size()));
        randomProject.click();
        element.ClickButton(element.ContinueImportButton);
        element.ClickButton(trello.BackButtonInImport);
        String sq = trello.TextOnProjectChoice.getText();
        Thread.sleep(500);
        if (sq.equals("Back\nSelect boards")) {
            element.ClickButton(trello.BackButtonInImport);
            Thread.sleep(300);
            String s = trello.TextOnImportChoice.getText();
            trello.Comparison(s,"Select Option to Import","второй бэк","ошибка, второй бэк");
        } else {
            System.out.println("ошибка, первый бэк");
        }


    }

    @Test
    public void CancelImportTrelloInProgress() throws Exception {
        System.out.println("Test :Завершение импорта когда импорт в прогрессе");


            trello.Registration(trello);
            element.Waiter(element.ListOfProjects);
            WebElement randomProject = trello.Projects.get(new Random().nextInt(trello.Projects.size()));
            randomProject.click();
            element.ClickButton(element.ContinueImportButton);
            trello.ClickButton(trello.SkipButtonTrello);
            element.ClickButton(element.OkGotItButtonAfterImport);
            element.ClickButton(element.UserMenuBlock);
            element.ClickButton(element.CompanySettingsButton);
            element.ClickButton(element.Import);
            Thread.sleep(3000);

            element.ClickButton(trello.CancelButtonInImportProgress);
            Thread.sleep(6000);
            String s1 = trello.TextOnImportChoice.getText();
            trello.Comparison(s1,"Select Option to Import","cancel after starting of import","ошибка");




    }




    @Test
    public void CancelImportBeforeChoiceOfProjects() throws Exception {
        System.out.println("Тест :Завершение импорта до того как загрузился список проектов");


        trello.Registration(trello);
        Thread.sleep(350);
        element.ClickButton(element.Cancel);
        Thread.sleep(2000);
        String s = trello.TextOnImportChoice.getText();
        trello.Comparison(s,"Select Option to Import","cancel after starting of import","ошибка" );



    }

    @After
    public void close(){
        driver.close();
    }


}