
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class JiraItems extends MainPage  {




    Files files = new Files();


    @FindBy(className = " select_option_item_jira")
    private WebElement ImportFromJiraButton;

    @FindBy(css="[formcontrolname='jiraDomain']")
    private WebElement JiraDomenLine;

    @FindBy(css="[formcontrolname='jiraUsername']")
    private WebElement JiraLoginLine;

    @FindBy(css="[formcontrolname='jiraPassword']")
    private WebElement JiraPasswordLine;

    @FindBy(css ="[class = 'btn login_btn btn_primary']")
    private WebElement LogInJiraButton;

    @FindBys(@FindBy(css = "div[class*='projects_item']"))
     List<WebElement> Projects;


    @FindBy(css ="[class = 'btn_primary']")
     WebElement StartImport;

    @FindBy(className ="select_projects_back")
    WebElement BackButtonInImport;

    @FindBy(className ="select_projects_title")
    WebElement TextOnProjectChoice;

    @FindBy(className ="select_option_title")
    WebElement TextOnImportChoice;


    public JiraItems(WebDriver driver) {
        super(driver);
    }

    public void Registration(JiraItems jira){
        jira.ClickButton(jira.ImportFromJiraButton);
        jira.SendText(jira.JiraDomenLine, jira.files.JiraDomain);
        jira.SendText(jira.JiraLoginLine, jira.files.JiraLogin);
        jira.SendText(jira.JiraPasswordLine, jira.files.JiraPassword);
        jira.ClickButton(jira.LogInJiraButton);

    }


}


