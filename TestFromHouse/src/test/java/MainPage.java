

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.List;
import java.util.NoSuchElementException;

public class MainPage{


    private WebDriver driver1;

    Files files = new Files();
    private WebDriverWait wait;

    public static String url = "";
    //public static String url = "";



    @FindBy(id = "mail")
    private WebElement MailAtlazLine;

    @FindBy(id = "password")
    private WebElement PasswordAtlazLine;

    @FindBy(className = "btn_primary")
    private WebElement LogInAtlazButton;

    @FindBy(className = "company_item")
    private WebElement SelectCompanyItem;




    @FindBy(className = " user_menu_block")
    public WebElement UserMenuBlock;

    @FindBy(linkText = "Company Settings")
    public WebElement CompanySettingsButton;

    @FindBy(css = "[href ='/company-settings/import']")
    public WebElement Import;


    @FindBy(css ="[class = 'btn_primary']")
    public WebElement ContinueImportButton;

    @FindBy(css ="[class = 'btn btn_primary']")
    public WebElement OkGotItButtonAfterImport;


    @FindBy(className = "projects_result")
    public WebElement ListOfProjects;

    @FindBy(css ="[ng-reflect-ng-switch = 'import']")
    public WebElement NotificationOfImport;

    @FindBy(css ="[class = 'btn_primary cancel_import_btn']")
    public WebElement Cancel;

    @FindBy(css ="div > import-notification-preview")
    public WebElement NotificationImport;






















    public MainPage(WebDriver driver) {
        this.driver1 = driver;
        wait = new WebDriverWait(driver, 80);
        PageFactory.initElements(driver1, this);

    }


    public  void SendText(WebElement element, String text){
        wait.until(ExpectedConditions.visibilityOf(element));

        element.clear();
        element.click();
        element.sendKeys(text);

    }
    //метод для логирования
    /*public void ClickButton(WebElement element, String text)
    {
        wait.until(ExpectedConditions.visibilityOf(element));
        System.out.println(text + " вошли в метод");
        element.click();
        System.out.println(text + " кликнули");
    }*/
    public void ClickButton(WebElement element)
    {
        wait.until(ExpectedConditions.visibilityOf(element));
        element.click();
    }

    public void Waiter(WebElement element)
    {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public boolean existsElement() {
        try {
            driver1.findElement(By.cssSelector("[ng-reflect-ng-switch = 'import']"));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void RegistrationInAtlaz(MainPage element){
        element.SendText(element.MailAtlazLine, element.files.MailAtlaz);
        element.SendText(element.PasswordAtlazLine, element.files.PasswordAtlaz);
        element.ClickButton(element.LogInAtlazButton);
        element.ClickButton(element.SelectCompanyItem);
    }







}


