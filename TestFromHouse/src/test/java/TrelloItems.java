
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.*;


public class TrelloItems extends MainPage {




    Files files = new Files();
    String TimeOfNotification = null;
    String TextOfNotification = null;


    @FindBy(className = "select_option_item_trello")
    private WebElement ImportFromTrelloButton;

    @FindBy(css ="[class = 'button primary']")
    private WebElement TrelloAutorizationButton;

    @FindBy(id = "user")
    private WebElement MailTrelloLine;

    @FindBy(id = "password")
    private WebElement PasswordTrelloLine;

    @FindBy(id = "login")
    private WebElement LoginTrelloButton;

    @FindBy(css ="[class = 'primary']")
    private WebElement TrelloAgreeButton;

    @FindBys(@FindBy(css = "div[class*='projects_item']"))
     List<WebElement> Projects;

    @FindBys(@FindBy(css = "div[class*='import_content_row']"))
     List<WebElement> ListOfUsers;

    @FindBy(css ="[class = 'btn_primary']")
     WebElement ContinueImportAfterSelectingUsers;


    @FindBy(css ="[class = 'btn_text']")
     WebElement SkipButtonTrello;

    @FindBy(className ="import_back")
     WebElement BackButtonInImport;

    @FindBy(className ="import_title")
     WebElement TextOnProjectChoice;

    @FindBy(className ="select_option_title")
     WebElement TextOnImportChoice;

    @FindBy(css ="[class = 'btn_primary cancel_import_btn']")
     WebElement CancelButtonInImportProgress;

    @FindBy(css ="div > import-notification-preview")
    public WebElement NotificationImport;







    public TrelloItems(WebDriver driver) {
        super(driver);

    }

    public void Registration(TrelloItems trello){
        trello.ClickButton(trello.ImportFromTrelloButton);
        trello.ClickButton(trello.TrelloAutorizationButton);
        trello.SendText(trello.MailTrelloLine, trello.files.TrelloLogin);
        trello.SendText(trello.PasswordTrelloLine, trello.files.TrelloPassword);
        trello.ClickButton(trello.LoginTrelloButton);
        trello.ClickButton(trello.TrelloAgreeButton);
    }

    public void CheckNotification(TrelloItems trello){
        for (; ;) {

            if (trello.existsElement() == true) {
                TimeOfNotification = trello.NotificationImport.findElement(By.cssSelector("[format = 'short']")).getText();

                if(TimeOfNotification.equals("just now"))
                {
                    trello.Waiter(trello.NotificationImport);
                    TextOfNotification = trello.NotificationImport.findElement(By.className("task_title")).getText();
                    if(TextOfNotification.equals("Trello import is failed.")){
                        System.out.println("Import is failed");
                    }
                    else {
                        System.out.println("Нотификация появилась");
                    }
                    WebElement archive = trello.NotificationImport.findElement(By.className("archive_notif_controll"));
                    archive.click();
                    break;
                }

            }
        }
    }

    public void ListOfInvatingUsers(TrelloItems trello, WebDriver driver) throws InterruptedException{
        for (int i = 17; i < trello.ListOfUsers.size(); i++) {
            WebElement a = trello.ListOfUsers.get(i);
            a.click();
            Thread.sleep(500);
            (new Actions(driver)).sendKeys(i + "tralala@ya.ru").build().perform();
        }
    }

    public void Comparison(String s,String s1,String s2,String s3){
        if(s.equals(s1))
        {
            System.out.println(s2);
        }
        else System.out.println(s3);

    }



}


