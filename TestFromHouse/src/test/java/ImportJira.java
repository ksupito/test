
import org.apache.log4j.Logger;
import org.junit.After;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.Test;
import org.junit.Before;



import java.util.*;


public class ImportJira {
    WebDriver driver;
    MainPage element;
    JiraItems jira;
    String TimeOfNotification = null;
    String TextOfNotification = null;
    private static final Logger log = Logger.getLogger(ImportJira.class);


    @Before
    public void LoginInAtlaz() throws Exception {
        System.setProperty("webdriver.chrome.driver", "d:\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(element.url);
        element = new MainPage(driver);
        jira = new JiraItems(driver);

        element.RegistrationInAtlaz(element);

        //код для выбора определенной компании
        /*for(int i = 1; i < 10; i ++){
            String a = "//choose-compnay/a[" + i + "]/div[2]";
            String NameOfElement = driver.findElement(By.xpath(a)).getText();
            if(NameOfElement.equals("ForImportKsu")){
                String aq = "//choose-compnay/a[" + i + "]";
                WebElement aaaaa =  driver.findElement(By.xpath(aq));
                aaaaa.click();
                break;}
        }*/
        element.ClickButton(element.UserMenuBlock);
        element.ClickButton(element.CompanySettingsButton);
        element.ClickButton(element.Import);

    }

    @Test
    public void ImportJira() throws Exception {
           jira.Registration(jira);

            element.Waiter(element.ListOfProjects);
            WebElement randomProject = jira.Projects.get(new Random().nextInt(jira.Projects.size()));
            randomProject.click();
            element.ClickButton(element.ContinueImportButton);

            jira.ClickButton(jira.StartImport);
            element.ClickButton(element.OkGotItButtonAfterImport);
        for (; ; ) {

            if (element.existsElement() == true) {

                TimeOfNotification = element.NotificationImport.findElement(By.cssSelector("[format = 'short']")).getText();
                if(TimeOfNotification.equals("just now"))
                {
                    element.Waiter(element.NotificationImport);
                    TextOfNotification = element.NotificationImport.findElement(By.className("task_title")).getText();
                    if(TextOfNotification.equals("Jira import is failed.")){
                        System.out.println("Import is failed");
                    }
                    else {
                        System.out.println("Нотификация появилась");
                    }
                    WebElement archive = element.NotificationImport.findElement(By.className("archive_notif_controll"));
                    archive.click();
                    break;
                }
            }
        }


    }

    @Test
    public void ImportJiraBackButton() throws Exception {
        //jira.Registration(jira);


        //element.Waiter(element.ListOfProjects);
        WebElement randomProject = jira.Projects.get(new Random().nextInt(jira.Projects.size()));
        randomProject.click();
        element.ClickButton(element.ContinueImportButton);

        element.ClickButton(jira.BackButtonInImport);
        String s = jira.TextOnProjectChoice.getText();
        System.out.println(s);
        Thread.sleep(500);
        if (s.equals("Back\n" +
                "Search projects\n" +
                "Help")) {
            element.ClickButton(jira.BackButtonInImport);
            Thread.sleep(300);
            String s1 = jira.TextOnImportChoice.getText();
            if (s1.equals("Select Option to Import")) {
                System.out.println("второй бэк");
            } else System.out.println("ошибка, второй бэк");
        } else {
            System.out.println("ошибка, первый бэк");
        }
        }

    @Test
    public void ImportJiraBeforeImport() throws Exception {
        jira.Registration(jira);

        Thread.sleep(350);
        element.ClickButton(element.Cancel);
        //проверка что тест в базе не упал
        Thread.sleep(2000);
        String s = jira.TextOnImportChoice.getText();

        if (s.equals("Select Option to Import")) {
            System.out.println("cancel after starting of import");
        } else System.out.println("ошибка");


    }


    @After
    public void close(){
        driver.close();
    }



}












